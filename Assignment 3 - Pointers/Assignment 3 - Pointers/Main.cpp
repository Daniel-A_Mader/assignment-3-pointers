
// Assignment 3 - Pointers
// Daniel Mader <Your Name>


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function
void SwapIntegers(int* one, int* two)
{
	int& placeholder1 = *one;
	int& placeholder2 = *two;
	int p1 = placeholder1;
	int p2 = placeholder2;
	*one = p2;
	*two = p1;

}

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	(void)_getch();
	return 0;
}
